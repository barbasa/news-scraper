# README #

This README are more note to self for the moment rather than proper instructions

### Scrapy and scrapyd ###

**Scrapyd** is a demon that handles the scraping of the different sources. Here the API documentation: http://scrapyd.readthedocs.org/en/latest/api.html#api

**scrapy** is the framework to implement the *spiders*.

### How do I get set up? ###

* Install Scrapy and scrapyd
* Configuration
* * scrapy.cfg: contains the location of the setting files of the project and the url of the deamon (scrapyd)
* * setup.py: contains the configurations parameters to build the egg
* Install scrapy-mongodb: https://github.com/sebdah/scrapy-mongodb
* Install scrapy-solr: https://github.com/scalingexcellence/scrapy-solr
* Run it
* * deamon: run *scrapyd* in the project directory
* * add scraper: scrapy deploy default -p newsscraper