from setuptools import setup, find_packages
setup(
    name = "newsscraper",
    version = "0.1",
    packages = find_packages(),
    # See http://stackoverflow.com/questions/14634733/scrapyd-deploy-attribute-error-nonetype-object-has-no-attribute-module-name
    # for the need of entry_point 
    entry_points={'scrapy': ['settings = newsscraper.settings']},
)
