# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class NewsscraperItem(scrapy.Item):
    headline = scrapy.Field()
    link = scrapy.Field()
    story = scrapy.Field()
    authorArticle = scrapy.Field()
    source = scrapy.Field()
    lead = scrapy.Field()
    pass
