# -*- coding: utf-8 -*-

# Scrapy settings for newsscraper project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

BOT_NAME = 'newsscraper'

SPIDER_MODULES = ['newsscraper.spiders']
NEWSPIDER_MODULE = 'newsscraper.spiders'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'newsscraper (+http://www.yourdomain.com)'

#ITEM_PIPELINES = [
#  'scrapy_mongodb.MongoDBPipeline',
#]
#
#MONGODB_URI = 'mongodb://localhost:27017'
#MONGODB_DATABASE = 'scrapy'
#MONGODB_COLLECTION = 'sources'
#MONGODB_ADD_TIMESTAMP = True

ITEM_PIPELINES = {
  'scrapysolr.SolrPipeline': 100,
}

SOLR_URL = 'http://192.168.33.10:8080/solr'
SOLR_MAPPING = {
  'id': 'link',
  'source': 'source',
  'authorArticle': 'authorArticle',
  'headline': 'headline',
  'lead': 'lead',
  'story': 'story',
  'link': 'link',
  'category': 'category'
}