import scrapy

from newsscraper.items import NewsscraperItem
from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors import LinkExtractor

class MatchFashionSpider(CrawlSpider):
    name = "matchfashion"
    allowed_domains = ["matchesfashion.com"]
    start_urls = [
        "http://www.matchesfashion.com/womens/bags?pagesize=60"
    ]

    rules = (
        Rule(LinkExtractor(allow=('.*?pagenumber=\d+', ), ), follow=True, callback="parse_item"),
    )


    def parse_item(self, response):
        for sel in response.xpath('//div[@class="product medium"]'):
            print "ciccio"
            item = NewsscraperItem()
            item['title'] = sel.xpath('h4/text()').extract()
            item['link'] = sel.css('img').xpath('@src').extract()
            item['desc'] = 'desc'
            print item
            yield item
