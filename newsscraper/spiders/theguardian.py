import scrapy

import time
import pprint

from scrapy.selector import Selector

from newsscraper.items import NewsscraperItem

class IndipendentSpider(scrapy.Spider):
    name = "theguardian"
    allowed_domains = ["www.theguardian.com"]
    start_urls = [
        "http://www.theguardian.com/world/argentina/rss"
    ]

    def parse(self, response):
        sel = Selector(response)
        news = sel.xpath('//rss/channel/item')
        items = []
        for n in news:
            item = NewsscraperItem()
            link = n.xpath('link/text()').extract()
            item['link'] = str(link[0])
            item['authorArticle'] = 'Random Guardian journalist'
            item['source'] = 'The Guardian Argentina news'
            detail_page_uri = item['link']

            request = scrapy.Request(detail_page_uri,
                          callback=self.parse_detail_page, dont_filter=True)
            request.meta['item'] = item
            yield request

    def parse_detail_page(self, response):
        item = response.meta['item']
        sel = Selector(response)

        headline = sel.xpath('//h1/text()').extract()
        item['headline'] = headline[0]

        lead = sel.xpath('//div[@id="stand-first"]/text()').extract()
        item['lead'] = lead[0]

        story = sel.xpath('//p/text()').extract()
        item['story'] = ' '.join(story)
        return item
