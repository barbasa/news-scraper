import scrapy

import time
import pprint

from scrapy.selector import Selector

from newsscraper.items import NewsscraperItem

class BBCSpider(scrapy.Spider):
    name = "bbc"
    allowed_domains = ["bbc.org"]
    start_urls = [
        "http://feeds.bbci.co.uk/news/world/latin_america/rss.xml",
        "http://feeds.bbci.co.uk/news/world/europe/rss.xml"
    ]

    def parse(self, response):
        sel = Selector(response)
        news = sel.xpath('//rss/channel/item')
        items = []
        for n in news:
            item = NewsscraperItem()
            link = n.xpath('link/text()').extract()
            item['link'] = str(link[0])
            item['authorArticle'] = 'Random BBC journalist'
            item['source'] = 'BBC news'
            detail_page_uri = item['link']

            request = scrapy.Request(detail_page_uri,
                          callback=self.parse_detail_page, dont_filter=True)
            request.meta['item'] = item
            yield request

    def parse_detail_page(self, response):
        item = response.meta['item']
        sel = Selector(response)

        detail = sel.xpath('//p[@class="introduction"]/text()').extract()
        item['lead'] = str(detail[0])

        headline = sel.xpath('//h1[@class="story-header"]/text()').extract()
        item['headline'] = str(headline[0])

        story = sel.xpath('//p/text()').extract()
        item['story'] = str(' '.join(story))
        return item
